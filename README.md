# Solid Calendar App

### Summary:

**Note**: This app is still in alpha stages of development.

Allows users to create and query events stored on their card in their solid pod. Users can add new events to their card through the add page and query events back by selecting a date on the calendar on the home page.

### Installiation and Setup:

Clone the repository, cd into directory, install dependencies, and run

```bash
 git clone https://dylanmartin@bitbucket.org/dylanmartin/solidcalendar.git
 cd solid-calendar
 yarn install
 npm run start
```



### Data Storage:

The data is stored on the users card using the common core ontology allowing data to be integrated with other common core conformant data. 

### Future Features:

- Store data in a document in the users private folder
- Edit events