import React, { Component } from 'react';
import WelcomePageContent from './welcome.component';
import { withWebId } from '@inrupt/solid-react-components';
import data from '@solid/query-ldflex';
import { withToastManager } from 'react-toast-notifications';


const fileClient = require('solid-file-client');
const defaultProfilePhoto = '/img/icon/empty-profile.svg';
var CUPurl;
const $rdf = require("rdflib");
const store = $rdf.graph();

/**
 * Container component for the Welcome Page, containing example of how to fetch data from a POD
 */
class WelcomeComponent extends Component<Props> {
  constructor(props) {
    super(props);

    this.state = {
      name: '',
      image: defaultProfilePhoto,
      isLoading: false,
      hasImage: false,
      selectedDate: new Date(),
    };
    this.isLoadingSetter = this.isLoadingSetter.bind(this);
    this.setDate = this.setDate.bind(this);
  }

  isLoadingSetter(bool){
    this.setState({isLoading:bool});
  }
  setDate(date){
    this.setState({selectedDate: date});
  }
  async componentDidMount() {
    if (this.props.webId) {
      this.setState({ isLoading: true });

    // CUPurl = this.props.webId.replace('profile/card#me', '') + 'private/event#';
    CUPurl = this.props.webId;
    let fetcher = new $rdf.Fetcher(store);
    // create cup file if not already exists
    try {
      await fetcher.load(CUPurl);
    }
    catch (error) {
      console.log(error);
      // auth.fetch(CUPurl, {
      //   method: 'PUT', // or 'PUT'
      //   body: '' // data can be `string` or {object}!
      // }).then(res => { return res; })
      //   .then((response) => { console.log('res is ' + response) })
      //   .catch(error => console.log(error));
      console.log(CUPurl);
      await fileClient.createFile(CUPurl, "", 'text/turtle');
      console.log("File Created...");
      // fileClient.updateFile("https://jacobmcconomy.solid.community/private/datfile" , "Jacob is cool", 'text/turtle');
      // console.log("File Updated...");
    }
      this.getProfileData(this.props.webId);
    }


  }

  async componentDidUpdate(prevProps, prevState) {
    if (this.props.webId && this.props.webId !== prevProps.webId) {
      
      CUPurl = this.props.webId.replace('profile/card#me', '') + 'private/event#';
    // CUPurl = this.props.webId;
    let fetcher = new $rdf.Fetcher(store);
    // create cup file if not already exists
    try {
      await fetcher.load(CUPurl);
    }
    catch (error) {
      await fileClient.createFile(CUPurl, "", 'text/turtle');
      console.log("File Created...");
      // fileClient.updateFile("https://jacobmcconomy.solid.community/private/datfile" , "Jacob is cool", 'text/turtle');
      // console.log("File Updated...");
    }
      this.getProfileData(this.props.webId);
    }

  }

  /*
   * This function retrieves a user's card data and tries to grab both the user's name and profile photo if they exist.
   *
   * This is an example of how to use the LDFlex library to fetch different linked data fields.
   */
  getProfileData = async (webid) => {
    
    let hasImage;


    /*
     * This is an example of how to use LDFlex. Here, we're loading the webID link into a user variable. This user object
     * will contain all of the data stored in the webID link, such as profile information. Then, we're grabbing the user.name property
     * from the returned user object.
     */
    const user = data[this.props.webId];
    const nameLd = await user.name;
    const name = nameLd ? nameLd.value : '';

    let imageLd = await user.image;
    imageLd = imageLd ? imageLd : await user.vcard_hasPhoto;

    let image;
    if (imageLd && imageLd.value) {
      image = imageLd.value;
      hasImage = true;
    } else {
      hasImage = false;
      image = defaultProfilePhoto;
    }
    /*
     * This is where we set the state with the name and image values. The user[hasPhotoContext] line of code is an example of
     * what to do when LDFlex doesn't have the full context. LDFlex has many data contexts already in place, but in case
     * it's missing, you can manually add it like we're doing here.
     *
     * The hasPhotoContext variable stores a link to the definition of the vcard ontology and, specifically, the #hasPhoto
     * property that we're using to store and link the profile image.
     *
     * For more information please go to: https://github.com/solid/query-ldflex
     */
    this.setState({ name, image, isLoading: false, hasImage });
  };

  /**
   * updatedPhoto will update the photo url on vcard file
   * this function will check if user has image or hasPhoto node if not
   * will just update it, the idea is use image instead of hasPhoto
   * @params{String} uri photo url
   */
  updatePhoto = async (uri: String, message) => {
    try {
      const { user } = data;
      this.state.hasImage
        ? await user.image.set(uri)
        : await user.image.add(uri);
      this.props.toastManager.add(['', message], {
        appearance: 'success'
      });
    } catch (error) {
      this.props.toastManager.add(['Error', error.message], {
        appearance: 'error'
      });
    }
  };

  render() {
    var { name, image, isLoading, selectedDate, data } = this.state;
    return (
      <WelcomePageContent
        name={name}
        image={image}
        isLoading={isLoading}
        webId={this.props.webId}
        updatePhoto={this.updatePhoto}
        selectedDate={selectedDate}
        loadSetter={this.isLoadingSetter}
        dateSetter={this.setDate}
        data={data}
      />
    );
  }

}

export default withWebId(withToastManager(WelcomeComponent));
