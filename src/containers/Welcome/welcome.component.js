import React from 'react';
//import { Uploader } from '@inrupt/solid-react-components';
import isLoading from '@hocs/isLoading';
import { /*Trans,*/ withTranslation } from 'react-i18next';
import {
  WelcomeWrapper,
  WelcomeCard,
  QueryCard
} from './welcome.style';
import { withToastManager } from 'react-toast-notifications';
//import { ImageProfile } from '@components';
import { Calendar } from 'react-calendar';
import $rdf from "rdflib";
//import { get } from 'https';
//import { keyframes } from 'emotion';
import './results.css';
//import { min } from 'moment';
import { deleteTriples } from '../../parse';
//import LoadingScreen from '../loadingSrceen';
//import { directive } from '@babel/types';
// temp local variable that stores moc data i would get back from the query 
// const mockData = [
//   {
//     "name": "Go out with friends",
//     "address": "123 Some Dr.",
//     "city": "Newark",
//     "state": "DE",
//     "zipcode": "19711"
//   }, {
//     "name": "Talk to Boss",
//     "address": "123 Big Boi Dr.",
//     "city": "Newark",
//     "state": "DE",
//     "zipcode": "19711"
//   }
// ]

const query = `select distinct ?SocialAct ?eventName ?eventDescription ?eventZipCode ?eventCity ?eventStreetAddress ?eventLocation ?eventState ?eventEndTime ?eventStartTime
where{
  ?SocialAct a <http://www.ontologyrepository.com/CommonCoreOntologies/SocialAct>.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?DesignativeName1.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/occurs_at> ?Facility.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/described_by> ?DiscriptiveInformationContentEntity.
  ?SocialAct <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/occurs_on> ?Time.
  ?DesignativeName1 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesignativeName1 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro1.
  ?Ro1 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro1 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/has_text_value> ?eventName.
  ?DiscriptiveInformationContentEntity a <http://www.ontologyrepository.com/CommonCoreOntologies/DescriptiveInformationContentEntity>.
  ?DiscriptiveInformationContentEntity <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro2.
  ?Ro2 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro2 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventDescription.
  ?Facility a <http://www.ontologyrepository.com/CommonCoreOntologies/Facility>.
  ?Facility <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?StreetAddress, ?DesignativeName3.
  ?Facility <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0001025> ?GeospatialRegion.
  ?GeospatialRegion a <http://www.ontologyrepository.com/CommonCoreOntologies/GeospatialRegion>.
  ?GeospatialRegion <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/BFO_0000050> ?PostalZone.
  ?GeospatialRegion <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/BFO_0000050> ?LocalAdministrativeRegion.
  ?PostalZone a  <http://www.ontologyrepository.com/CommonCoreOntologies/PostalZone>.
  ?PostalZone <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?ZipCode.
  ?ZipCode <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro3.
  ?Ro3 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro3 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventZipCode.
  ?LocalAdministrativeRegion a <http://www.ontologyrepository.com/CommonCoreOntologies/LocalAdministrativeRegion>.
  ?LocalAdministrativeRegion <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?DesignativeName2.
  ?LocalAdministrativeRegion <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/BFO_0000050> ?State.
  ?DesignativeName2 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesignativeName2 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro4.
  ?Ro4 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventCity.
  ?StreetAddress a <http://www.ontologyrepository.com/CommonCoreOntologies/StreetAddress>.
  ?StreetAddress <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro5.
  ?Ro5 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro5 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventStreetAddress.
  ?DesignativeName3 a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?DesignativeName3 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro6.
  ?Ro6 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro6 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventLocation.
  ?State a <http://www.ontologyrepository.com/CommonCoreOntologies/State>.
  ?State <http://www.ontologyrepository.com/CommonCoreOntologies/designated_by> ?AbbreviatedName.
  ?AbbreviatedName a <http://www.ontologyrepository.com/CommonCoreOntologies/DesignativeName>.
  ?AbbreviatedName <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro7.
  ?Ro7 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro7 <http://www.ontologyrepository.com/CommonCoreOntologies/has_text_value> ?eventState.
  ?Time a <http://purl.obolibrary.org/obo/BFO_0000038>.
  ?Time <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/interval_finished_by> ?EndTime.
  ?Time <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/interval_started_by> ?StartTime.
  ?EndTime a <http://purl.obolibrary.org/obo/BFO_0000038>.
  ?EndTime <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/designated_by> ?CodeIdentifier1.
  ?CodeIdentifier1 a <http://www.ontologyrepository.com/CommonCoreOntologies/CodeIdentifier>.
  ?CodeIdentifier1 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro8.
  ?Ro8 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro8 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/has_datetime_value> ?eventEndTime.
  ?StartTime a <http://purl.obolibrary.org/obo/BFO_0000038>.
  ?StartTime <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/designated_by> ?CodeIdentifier2.
  ?CodeIdentifier2 a <http://www.ontologyrepository.com/CommonCoreOntologies/CodeIdentifier>.
  ?CodeIdentifier2 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/RO_0010001> ?Ro9.
  ?Ro9 a <http://www.ontologyrepository.com/CommonCoreOntologies/InformationBearingEntity>.
  ?Ro9 <http://www.ontologyrepository.com/CommonCoreOntologies/ModalRelationOntology/has_datetime_value> ?eventStartTime.
}`;

var template;
var CUPurl;
var isLoadingSetter;
//var dispEvent;

fetch('./eventSlim.nt').then(
  (r) => r.text()
).then(
  (text) => template = text
);


// RG - 2019-02-28
// Loads the data from a URL into the local store
const loadFromUrl = (url, store) => {
  return new Promise((resolve, reject) => {
    let fetcher = new $rdf.Fetcher(store);
    try {
      fetcher.load(url).then(response => {
        resolve(response.responseText);
        // console.debug(response.responseText);
        // $rdf.parse(response.responseText, store, $rdf.sym(url).uri,"application/rdf");
      });
    } catch (err) {
      reject(err);
    }
  });
};

// RG - 2019-02-28
// Prepares a query by converting SPARQL into a Solid query
const prepare = (qryStr, store) => {
  return new Promise((resolve, reject) => {
    try {
      let query = $rdf.SPARQLToQuery(qryStr, false, store);
      resolve(query);
    } catch (err) {
      reject(err);
    }
  });
};

// RG - 2019-02-28
// Executes a query on the local store
const execute = (qry, store) => {
  return new Promise((resolve, reject) => {
    // console.debug("here");
    const wanted = qry.vars;
    const resultAry = [];
    store.query(
      qry,
      results => {
        // console.debug("here1");
        if (typeof results === "undefined") {
          reject("No results.");
        } else {
          let row = rowHandler(wanted, results);
          // console.debug(row);
          if (row) resultAry.push(row);
        }
      },
      {},
      () => {
        resolve(resultAry);
      }
    );
  });
};

// RG - 2019-02-28
// Puts query results into an array according to the projection
const rowHandler = (wanted, results) => {
  const row = {};
  for (var r in results) {
    let found = false;
    let got = r.replace(/^\?/, "");
    if (wanted.length) {
      for (var w in wanted) {
        if (got === wanted[w].label) {
          found = true;
          continue;
        }
      }
      if (!found) continue;
    }
    row[got] = results[r].value;
  }
  return row;
};

async function getData(date) {
  // loading new events
  let store = $rdf.graph();
  isLoadingSetter(true);
  //console.log(query);
  loadFromUrl(CUPurl, store).then(() =>
    prepare(query, store).then(qry =>
      execute(qry, store).then(results => {
        let filteredRes = []
        for (let x = 0; x < results.length; x++) {
          let eventDate = new Date(results[x].eventStartTime);
          if (eventDate.getDate() === date.getDate()) {
            filteredRes.push(results[x]);
          }
        }
        //console.log(filteredRes);
        isLoadingSetter(false);
        makeComponents(filteredRes);
        // loading done
      })
    )
  );
}


/**
 * Welcome Page UI component, containing the styled components for the Welcome Page
 * Image component will get theimage context and resolve the value to render.
 * @param props
 */
// const iterateProps = row => {
//   var str = "";
//   for (const k in row) {
//     if (row.hasOwnProperty(k)) {
//       if (str.length > 0) str += "; ";
//       str += row[k];
//     }
//   }
//   return str;
// };

// const displayEventSetter = (bool) => {
//     dispEvent = bool;
// }

// const getResults = results => {
//   if (results) {
//     return results.map(r => <p>{iterateProps(r)}</p>);
//   } else {
//     return "No results";
//   }
// };

async function getDate(value) {
  getData(value);
  // document.getElementById("datediv").innerHTML = "the selected date is: " + value.toString();
}

const ingestData = (template, data, hash) => {
  let ingestedData = '';
  let templateArr = template.split('\n');
  // creates a random hash to make uri unique
  for (let i = 0; i < templateArr.length; i++) {
    let line = templateArr[i];
    let placeHolder = templateArr[i].split(" ")[2];
    for (let x = 0; x < line.split(" ").length; x++) {
      if (line.split(' ')[x].startsWith('_:')) {
        line = line.replace(line.split(' ')[x], line.split(' ')[x] + hash );
        
      }

    }
    switch (placeHolder) {
      case '"address_value"':
        ingestedData += line.replace(placeHolder, '"' + data.address + '"') + '\n';
        break;
      case '"start_time_value"':
        ingestedData += line.replace(placeHolder, JSON.stringify(data.startTime)) + '\n';
        break;
      case '"name_value"':
        ingestedData += line.replace(placeHolder, '"' + data.name + '"') + '\n';
        break;
      case '"end_time_value"':
        ingestedData += line.replace(placeHolder, JSON.stringify(data.stopTime)) + '\n';
        break;
      case '"description_value"':
        ingestedData += line.replace(placeHolder, '"' + data.description + '"') + '\n';
        break;
      case '"zip_code_value"':
        ingestedData += line.replace(placeHolder, '"' + data.zipcode + '"') + '\n';
        break;
      case '"location_value"':
        ingestedData += line.replace(placeHolder, '"' + data.location + '"') + '\n';
        break;
      case '"state_name"':
        ingestedData += line.replace(placeHolder, '"' + data.state + '"') + '\n';
        break;
      case '"city_name"':
        ingestedData += line.replace(placeHolder, '"' + data.city + '"') + '\n';
        break;
      default:
        ingestedData += line + '\n';
        break;
    }
  }
  //console.log(ingestedData);
  return ingestedData;
}
const makeDelete = (delButton, container, hash, addr, startTime, name, endTime, description, zip, location, state, city) =>{
  delButton.onclick = async () => {
    container.innerHTML = '';
    isLoadingSetter(true);
    let eventHash = hash;
    // let data = {
    //   address: addr,
    //   startTime: startTime,
    //   name: name,
    //   stopTime: endTime,
    //   description: description,
    //   zipcode: zip,
    //   location: location,
    //   state: state,
    //   city: city,
    // }
    let dataToDelete = ingestData(template, eventHash);
    await deleteTriples(CUPurl, hash);
    isLoadingSetter(false);
    return dataToDelete;
  }
}
const makeComponents = (arr) => {
  document.getElementById('eventsDiv').innerHTML = '';
  //let newArr = [];
  // If there are no events for the selected date then say so
  if (arr.length === 0) {
    document.getElementById('eventsDiv').innerHTML = 'There are no events for today';
    return;
  }
  for (let i = 0; i < arr.length; i++) {
    let obj = arr[i];
    let hash = obj.SocialAct;
    
    
    let name = obj.eventName;
    let addr = obj.eventStreetAddress;
    let city = obj.eventCity;
    let state = obj.eventState;
    let zip = obj.eventZipCode;
    let description = obj.eventDescription;
    let startTime = obj.eventStartTime;
    let endTime = obj.eventEndTime;
    let location = obj.eventLocation;
    let container = document.createElement('div');
    container.id = `container${i}`
    container.className = 'container';
    container.style.padding = '20px';
    document.getElementById('eventsDiv').appendChild(container);
    let row = document.createElement('div');
    row.className = 'row';
    row.id = `row${i}`;
    container.appendChild(row);
    let event = document.createElement('div');
    event.id = `event${i}`;
    event.className = 'event'
    document.getElementById(`row${i}`).appendChild(event);
    let nameDat = document.createElement('div');
    nameDat.innerHTML = `Name: ${name}`;
    document.getElementById(`event${i}`).appendChild(nameDat);
    let addrDat = document.createElement('div');
    addrDat.innerHTML = `Address: ${addr}`;
    document.getElementById(`event${i}`).appendChild(addrDat);
    let cityDat = document.createElement('div');
    cityDat.innerHTML = `City: ${city}`;
    document.getElementById(`event${i}`).appendChild(cityDat);
    let stateDat = document.createElement('div');
    stateDat.innerHTML = `State: ${state}`;
    document.getElementById(`event${i}`).appendChild(stateDat);
    let zipDat = document.createElement('div');
    zipDat.innerHTML = `Zipcode: ${zip}`;
    document.getElementById(`event${i}`).appendChild(zipDat);
    let startDat = document.createElement('div');
    startDat.innerHTML = `Start Time: ${new Date(startTime).toDateString()}`;
    document.getElementById(`event${i}`).appendChild(startDat);
    let stopDat = document.createElement('div');
    stopDat.innerHTML = `End Time: ${new Date(endTime).toDateString()}`;
    document.getElementById(`event${i}`).appendChild(stopDat);
    let descriptionDiv = document.createElement('div');
    descriptionDiv.innerHTML = `Description: ${description}`;
    document.getElementById(`event${i}`).appendChild(descriptionDiv);
    let buttonDiv = document.createElement('div');
    buttonDiv.id = `buttondiv`;
    document.getElementById(`row${i}`).appendChild(buttonDiv);
    let delButton = document.createElement('div');
    // delButton.className = 'col-6';
    delButton.id = 'delButton';
    delButton.innerHTML = "Remove";
    makeDelete(delButton, container, hash, addr, startTime, name, endTime, description, zip, location, state, city);
    buttonDiv.appendChild(delButton);

    // let minImg = document.createElement('img');
    // minImg.src = process.env.PUBLIC_URL + '/img/minus-128.png';
    // minImg.className = 'img';
    // delButton.appendChild(minImg);
  }
}


const WelcomePageContent = props => {
  var { name, t } = props;
  isLoadingSetter = props.loadSetter;
  var sd = props.selectedDate;
  if (props.webId !== undefined) {
    CUPurl = props.webId.replace('profile/card#me', '') + 'private/event#';
    // CUPurl = props.webId;
  }
  return (
    // props.isLoading ? <WelcomeWrapper data-testid="welcome-wrapper"><LoadingScreen /> </WelcomeWrapper> :
    <WelcomeWrapper data-testid="welcome-wrapper">
      <WelcomeCard className="card">
        <h3>
          {t('welcome.welcome')}, <span>{name}</span>
        </h3>
      </WelcomeCard>
      <WelcomeCard className="card">
        <Calendar value={sd} style={{ width: '100%' }} onClickDay={(value) => { props.dateSetter(value); getDate(value); }} />
      </WelcomeCard>
      <QueryCard className='card'>
        {/* <pre id='querydiv' style={{maxHeight:'100%',maxWidth:'100%',overflow:'scroll'}} /> */}
        <div id='eventsDiv'>
        </div>
      </QueryCard>
    </WelcomeWrapper>
  );
};


export { WelcomePageContent };
export default withTranslation()(
  isLoading(withToastManager(WelcomePageContent))
);
