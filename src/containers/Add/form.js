import React from "react";
//import DateTimePicker from 'react-datetime-picker';
import DateTime from 'react-datetime';
import './form.css';
import auth from "solid-auth-client";
import { run } from '../../parse.js';
import LoadingScreen from '../loadingSrceen'
var CUPurl;
const $rdf = require("rdflib");
export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoading:false
        };
        CUPurl = this.props.webid.replace('profile/card#me','') + 'private/event#';
        // CUPurl = this.props.webid;
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleStartTime = this.handleStartTime.bind(this);
        this.handleEndTime = this.handleEndTime.bind(this);
        this.ingestData = this.ingestData.bind(this);
        this.createHash = this.createHash.bind(this);
    }

    handleChange(event) {
        var name = event.target.name;
        var value = event.target.value;
        this.setState({
            [name]: value
        });
        console.log("State is " + JSON.stringify(this.state))
    }

    handleStartTime(value) {
        this.setState({
            startTime: value
        })
        console.log("State is " + JSON.stringify(this.state))
    }
    createHash(length) {
        var result = '';
        var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for (var i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }
    ingestData(template, data) {
        let ingestedData = '';
        let templateArr = template.split('\n');
        // creates a random hash to make uri unique
        
        for (let i = 0; i < templateArr.length; i++) {
            
            let line = templateArr[i];
            let placeHolder = templateArr[i].split(" ")[2];
            for (let x = 0; x < line.split(" ").length; x++) {
                if (line.split(' ')[x].startsWith('_:')) {
                    line = line.replace(line.split(' ')[x], line.split(' ')[x]);
                    
                }
            }
            switch (placeHolder) {
                case '"address_value"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.address + '"') + '\n';
                    break;
                case '"start_time_value"':
                    ingestedData += line.replace(placeHolder, JSON.stringify(this.state.startTime)) + '\n';
                    break;
                case '"name_value"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.name + '"') + '\n';
                    break;
                case '"end_time_value"':
                    ingestedData += line.replace(placeHolder, JSON.stringify(this.state.stopTime)) + '\n';
                    break;
                case '"description_value"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.description + '"') + '\n';
                    break;
                case '"zip_code_value"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.zipcode + '"') + '\n';
                    break;
                case '"location_value"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.location + '"') + '\n';
                    break;
                case '"state_name"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.state + '"') + '\n';
                    break;
                case '"city_name"':
                    ingestedData += line.replace(placeHolder, '"' + this.state.city + '"') + '\n';
                    break;
                default:
                    ingestedData += line + '\n';
                    break;
            }
        }
        return ingestedData;
    }


    handleEndTime(value) {
        this.setState({
            stopTime: value
        })
        console.log("State is " + JSON.stringify(this.state))
    }

    async handleSubmit(event) {
        event.preventDefault();
        event.stopPropagation();
        // show that the data is loading
        document.getElementById('state').innerHTML = '';
        let store = $rdf.graph();
        let fetcher = new $rdf.Fetcher(store);
        // create cup file if not already exists
        try {
            await fetcher.load(CUPurl);
        }
        catch (error) {
            console.log(error);
            auth.fetch(CUPurl, {
                method: 'PUT', // or 'PUT'
                body: '' // data can be `string` or {object}!
            }).then(res => { return res; })
                .then((response) => { console.log('res is ' + response) })
                .catch(error => console.log(error));
        }
        // make sure all fields are filled out
        if (this.state.name === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the name field';
            return;
        }
        else if (this.state.address === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the address field';
            return;
        }
        else if (this.state.city === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the city field';
            return;
        }
        else if (this.state.state === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the state field';
            return;
        }
        else if (this.state.zipcode === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the zipcode field';
            return;
        }
        else if (this.state.startTime === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the start time field';
            return;
        }
        else if (this.state.stopTime === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the stop time field';
            return;
        } else if (this.state.location === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please fill out the location field';
            return;
        } else if (this.state.description === undefined) {
            await this.setState({ isLoading: false });
            document.getElementById('state').innerHTML = 'Please write a description of the event';
            return;
        }
        await this.setState({ isLoading: true });
        await new Promise(
            async (resolve, reject) => {
                fetch('./eventSlim.nt').then(
                    r => r.text()
                ).then(
                    text => this.ingestData(text, this.state)
                ).then(
                    async text => {
                        //console.log('text is ' + text);
                        await run(text, 'socialact', CUPurl)
                        resolve()
                    }
                )
                    
            }
        )
        // app is done loading data
        await this.setState({ isLoading: false });
        // await run('../john-doe-karma-slim-v4.nt',CUPurl)
        // let x = await deleteStore().then(console.log(x));
    }
    render() {
        return (
            this.state.isLoading ? <LoadingScreen /> :
                <div className='mainDiv'>
                    <form onSubmit={this.handleSubmit}>
                        <div>
                            <label>Name:<input name="name" type='text' onChange={this.handleChange} /></label>
                            <label>Location: <input name='location' type='text' onChange={this.handleChange} /> </label>
                            <label>Address:<input name='address' type='text' onChange={this.handleChange} /></label>
                            <label>City:<input name='city' type='text' onChange={this.handleChange} /></label>
                            <label>State:<input name='state' type='text' onChange={this.handleChange} /></label>
                            <label>Zipcode:<input name='zipcode' type='text' onChange={this.handleChange} /></label>
                            <label>Start Time:</label> <DateTime utc={true} className='startTime' onChange={this.handleStartTime} />
                            <label>End Time:</label> <DateTime utc={true} className='endTime' onChange={this.handleEndTime} />
                            <label>Description:</label> <textarea name='description' onChange={this.handleChange} />
                        </div>
                        <div>
                            <input className='submitButton' type='submit'></input>
                        </div>
                        <span id='state'></span>
                    </form>
                </div>
        );
    }
}

