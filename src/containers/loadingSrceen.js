import React from 'react';

export default class LoadingScreen extends React.Component {

    render(){
        return(
            <div>
                <h1>
                    Loading Please Wait...
                </h1>
            </div>
        );

    }
}